package com.hanna.products.presenter.viewmodel

import androidx.lifecycle.*
import com.hanna.products.data.api.Resource
import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.domain.models.dto.model.ProductType
import com.hanna.products.domain.models.entities.ProductSummary
import com.hanna.products.domain.usecases.GetProductsSummaryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(
    private val getProductsSummaryUseCase: GetProductsSummaryUseCase,
) : ViewModel() {

    private val selectedProductType: MutableLiveData<ProductType> =
        MutableLiveData(ProductType.DISHWASHERS)

    val products: MutableLiveData<Resource<List<ProductSummary>>> =
        MutableLiveData<Resource<List<ProductSummary>>>(Resource.loading(null))

    val productsSummary: LiveData<Resource<List<ProductSummary>>> =
        Transformations.map(products) {
            return@map it
        }

    init {
        fetchProducts()
    }

    fun fetchProducts() {
        products.postValue(Resource.loading(null))
        viewModelScope.launch {
            val result: ResponseWrapper<List<ProductSummary>> =
                getProductsSummaryUseCase(selectedProductType.value ?: ProductType.DISHWASHERS)
            when (result) {
                is ResponseWrapper.Success -> products.postValue(Resource.success(result.value))
                is ResponseWrapper.Error -> products.postValue(
                    Resource.error(
                        result.throwable?.message.orEmpty(),
                        null
                    )
                )
            }
        }
    }
}