package com.hanna.products.domain

import com.hanna.products.base.BaseTest
import com.hanna.products.data.repository.ProductsRepository
import com.hanna.products.domain.usecases.GetProductDetailsByIdUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@OptIn(ExperimentalCoroutinesApi::class)
class GetProductDetailsByIdUseCaseTest : BaseTest() {

    private val productsRepository = mock<ProductsRepository>()
    private lateinit var getProductDetailsByIdUseCase: GetProductDetailsByIdUseCase

    @Before
    fun setUp() {
        getProductDetailsByIdUseCase = GetProductDetailsByIdUseCase(productsRepository)
    }

    @Test
    fun `when GetProductDetailsByIdUseCase is invoked, repository calls getProductDetails`() =
        runSuspendTest {
            getProductDetailsByIdUseCase.invoke("1234")
            verify(productsRepository).getProductDetails("1234")
        }
}