package com.hanna.products.presenter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hanna.products.data.api.Resource
import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.domain.models.entities.ProductDetailsEntity
import com.hanna.products.domain.usecases.GetProductDetailsByIdUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductDetailsViewModel @Inject constructor(
    private val getProductDetailsByIdUseCase: GetProductDetailsByIdUseCase,
) : ViewModel() {

    var productInfo: MutableLiveData<Resource<ProductDetailsEntity>> = MutableLiveData()

    fun fetchProductDetails(productId: String) {
        productInfo.postValue(Resource.loading(null))
        viewModelScope.launch {
            when (val result: ResponseWrapper<ProductDetailsEntity> =
                getProductDetailsByIdUseCase(productId)) {
                is ResponseWrapper.Success -> productInfo.postValue(Resource.success(result.value))
                is ResponseWrapper.Error -> productInfo.postValue(
                    Resource.error(
                        result.throwable?.message.orEmpty(),
                        null
                    )
                )
            }
        }
    }
}