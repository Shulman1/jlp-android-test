package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Brand(

    @SerializedName("logo") var logo: String? = null,
    @SerializedName("name") var name: String? = null

)