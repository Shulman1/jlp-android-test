package com.hanna.products.domain

import com.hanna.products.base.BaseTest
import com.hanna.products.data.repository.ProductsRepository
import com.hanna.products.domain.models.dto.model.ProductType
import com.hanna.products.domain.usecases.GetProductsSummaryUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class GetProductsSummaryUseCaseTest : BaseTest() {

    private val productsRepository = mock<ProductsRepository>()
    private lateinit var getProductDetailsByIdUseCase: GetProductsSummaryUseCase

    @Before
    fun setUp() {
        getProductDetailsByIdUseCase = GetProductsSummaryUseCase(productsRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `when GetProductDetailsByIdUseCase is invoked, repository calls getProductDetails`() =
        runSuspendTest {
            getProductDetailsByIdUseCase.invoke(ProductType.DISHWASHERS)
            verify(productsRepository).getProductsSummary(ProductType.DISHWASHERS)
        }
}