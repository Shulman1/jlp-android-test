package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Media (

  @SerializedName("videos" ) var videos : Videos? = Videos(),
  @SerializedName("images" ) var images : Images? = Images()

)