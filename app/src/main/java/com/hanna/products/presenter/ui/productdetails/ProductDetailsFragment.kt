package com.hanna.products.presenter.ui.productdetails

import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.hanna.products.R
import com.hanna.products.data.api.Resource
import com.hanna.products.databinding.FragmentProductDetailsBinding
import com.hanna.products.domain.models.entities.ProductDetailsEntity
import com.hanna.products.presenter.alerts.AlertDialogs
import com.hanna.products.presenter.viewmodel.ProductDetailsViewModel
import com.hanna.products.utils.ui.ImageLoader
import com.hanna.products.utils.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProductDetailsFragment : Fragment(R.layout.fragment_product_details) {

    private val viewModel by viewModels<ProductDetailsViewModel>()

    private val binding: FragmentProductDetailsBinding by viewBinding(FragmentProductDetailsBinding::bind)

    @Inject
    lateinit var imageLoader: ImageLoader

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchProductDetails(arguments?.getString(PRODUCT_ID).orEmpty())
        viewModel.productInfo.observe(viewLifecycleOwner) {
            binding.loadingPage.root.isVisible = it.status == Resource.Status.LOADING
            when (it.status) {
                Resource.Status.SUCCESS -> bindData(it.data)
                Resource.Status.ERROR -> {
                    AlertDialogs.showSimpleAlert(requireContext(), it.message.orEmpty()) {
                        viewModel.fetchProductDetails(arguments?.getString(PRODUCT_ID).orEmpty())
                    }
                }
                else -> {}
            }
        }
    }

    private fun bindData(data: ProductDetailsEntity?) {
        //I am not convinced the expected data has been chosen to be displayed, but with the implemented
        //architecture, updating this would take no longer than 2 minute :)
        data?.let { details ->
            details.services?.let {
                it.includedServices.forEach {
                    val textView = TextView(requireContext())
                    textView.setTextColor(Color.GREEN)
                    textView.text = it
                    binding.servicesContainer.addView(textView)
                }
            }
            imageLoader.loadImage(
                details.images[0].takeIf { it.isNotEmpty() }.orEmpty(),
                binding.productDetailImage
            )
            binding.productDetailImage.contentDescription = "Image of ${data.productTitle}"

            binding.productDetailsTitle.text = details.productTitle
            binding.productDetailsPrice.text = details.displayedProductPrice
            binding.specialOffer.text = details.displaySpecialOffer
            binding.specialOffer.isVisible = details.displaySpecialOffer.isNotEmpty()
            binding.productCode.text = getString(R.string.product_id_display, details.productId)
            binding.productCode.isVisible = details.productId.isNotEmpty()

            binding.productDetailDescription.text =
                Html.fromHtml(details.productInformation, Html.FROM_HTML_MODE_COMPACT)
        }
    }

    companion object {

        const val PRODUCT_ID = "product_id"
    }
}