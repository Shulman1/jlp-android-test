package com.hanna.products.data.mapper

import com.hanna.products.domain.models.dto.model.ProductResponse
import com.hanna.products.domain.models.entities.ProductSummary
import javax.inject.Inject

class ProductMapper @Inject constructor() {

    fun map(productResponse: ProductResponse): ProductSummary {
        return ProductSummary(
            productId = productResponse.productId,
            title = productResponse.title,
            imageUrl = "https:${productResponse.image}",
            displayPrice = productResponse.variantPriceRange.display.let { displayResponse ->
                "${displayResponse.min}-${displayResponse.max}".takeIf { displayResponse.min != displayResponse.max } ?: displayResponse.max
            }//due to missing requirements, I have just displayed the max' if would define as displaying range etc, this would be updated here
        )
    }
}