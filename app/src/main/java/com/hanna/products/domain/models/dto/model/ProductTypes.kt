package com.hanna.products.domain.models.dto.model

enum class ProductType(val productName: String) {
    DISHWASHERS("dishwasher")
}