package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName
import com.google.gson.internal.LinkedTreeMap
import org.json.JSONObject


data class ProductDetailsResponse(

    @SerializedName("legs") var legs: ArrayList<String> = arrayListOf(),
    @SerializedName("ageRestriction") var ageRestriction: Int? = null,
    @SerializedName("brand") var brand: Brand? = Brand(),
    @SerializedName("type") var type: String? = null,//perhaps should be pre-defined
    @SerializedName("details") var details: Details? = Details(),
    @SerializedName("skus") var skus: ArrayList<Skus> = arrayListOf(),
    @SerializedName("promotionalFeatures") var promotionalFeatures: ArrayList<JSONObject> = ArrayList(),
    @SerializedName("priceBands") var priceBands: ArrayList<String> = arrayListOf(),
    @SerializedName("storeOnly") var storeOnly: Boolean? = null,
    @SerializedName("averageRating") var averageRating: Double? = null,
    @SerializedName("isFBL") var isFBL: Boolean? = null,
    @SerializedName("defaultSku") var defaultSku: String? = null,
    @SerializedName("deliveries") var deliveries: ArrayList<Deliveries> = arrayListOf(),
    @SerializedName("numberOfReviews") var numberOfReviews: Int? = null,
    @SerializedName("media") var media: Media? = Media(),
    @SerializedName("price") var price: Price? = Price(),
    @SerializedName("productId") var productId: String? = null,
    @SerializedName("nonPromoMessage") var nonPromoMessage: String? = null,
    @SerializedName("emailMeWhenAvailable") var emailMeWhenAvailable: Boolean? = null,
    @SerializedName("specialOfferBundles") var specialOfferBundles: ArrayList<String> = arrayListOf(),
    @SerializedName("additionalServices") var additionalServices: AdditionalServices? = AdditionalServices(),
    @SerializedName("displaySpecialOffer") var displaySpecialOffer: String? = null,
    @SerializedName("templateType") var templateType: String? = null,
    @SerializedName("crumbs") var crumbs: ArrayList<Crumbs> = arrayListOf(),
    @SerializedName("dynamicAttributes") var dynamicAttributes: LinkedTreeMap<String, Any>,//DynamicAttributes? = DynamicAttributes(),
    @SerializedName("releaseDateTimestamp") var releaseDateTimestamp: Int? = null,
    @SerializedName("parentCategories") var parentCategories: ArrayList<String> = arrayListOf(),
    @SerializedName("bundleProducts") var bundleProducts: ArrayList<String> = arrayListOf(),
    @SerializedName("specialOffers") var specialOffers: SpecialOffers? = SpecialOffers(),
    @SerializedName("code") var code: String? = null,
    @SerializedName("lifeCycleState") var lifeCycleState: String? = null,
    @SerializedName("isAsafShape") var isAsafShape: Boolean? = null,
    @SerializedName("defaultCategory") var defaultCategory: DefaultCategory? = DefaultCategory(),
    @SerializedName("seoURL") var seoURL: String? = null,
    @SerializedName("title") var title: String? = null

)