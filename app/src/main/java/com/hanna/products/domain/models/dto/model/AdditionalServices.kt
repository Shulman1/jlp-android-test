package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class AdditionalServices(

    @SerializedName("optionalServices") var optionalServices: ArrayList<OptionalServices> = arrayListOf(),
    @SerializedName("includedServices") var includedServices: ArrayList<String> = arrayListOf()

)