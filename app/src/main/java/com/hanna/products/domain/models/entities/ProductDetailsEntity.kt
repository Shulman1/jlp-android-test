package com.hanna.products.domain.models.entities

import com.hanna.products.domain.models.dto.model.AdditionalServices
import com.google.gson.internal.LinkedTreeMap

data class ProductDetailsEntity(
    val images: List<String>,
    val productTitle: String,
    val displayedProductPrice: String,
    val productInformation: String,
    val productAttributes: LinkedTreeMap<String, Any>,
    val displaySpecialOffer: String,
    val services: AdditionalServices?,
    val productId: String
)