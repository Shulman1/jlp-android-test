package com.hanna.products.presenter.ui.productdetails

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.commit
import com.hanna.products.R
import com.hanna.products.presenter.ui.productdetails.ProductDetailsFragment.Companion.PRODUCT_ID
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val productId = intent.getStringExtra(EXTRA_PRODUCT_INFO_ID)

        supportFragmentManager.commit {
            replace(
                R.id.fragment_container,
                ProductDetailsFragment::class.java,
                bundleOf(PRODUCT_ID to productId)
            )
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        const val EXTRA_PRODUCT_INFO_ID = "product_info_id"
    }
}