package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Crumbs(

    @SerializedName("item") var item: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("displayName") var displayName: String? = null,
    @SerializedName("clickable") var clickable: String? = null

)