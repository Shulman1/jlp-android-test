package com.hanna.products.data.repository

import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.domain.models.dto.model.ProductType
import com.hanna.products.domain.models.entities.ProductDetailsEntity
import com.hanna.products.domain.models.entities.ProductSummary

interface ProductsRepository {

    suspend fun getProductsSummary(productType: ProductType): ResponseWrapper<List<ProductSummary>>

    suspend fun getProductDetails(productId: String): ResponseWrapper<ProductDetailsEntity>
}