package com.hanna.products.data.repository

import com.hanna.products.data.api.ProductsApi
import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.data.mapper.ProductDescriptionMapper
import com.hanna.products.data.mapper.ProductMapper
import com.hanna.products.data.network.NetworkResponse
import com.hanna.products.domain.models.dto.model.ProductType
import com.hanna.products.domain.models.entities.ProductDetailsEntity
import com.hanna.products.domain.models.entities.ProductSummary
import javax.inject.Inject

class ProductsRepositoryImpl @Inject constructor(
    private val productsApi: ProductsApi,
    private val productMapper: ProductMapper,
    private val productDetailsMapper: ProductDescriptionMapper,
) : ProductsRepository {

    private fun getUserToken(): String {
        //ideally an hashed/hidden token would be stored in the preferences, with a defined way to
        // resolve the suggested key.
        return "AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
    }

    override suspend fun getProductsSummary(productType: ProductType): ResponseWrapper<List<ProductSummary>> {
        return when (val response =
            productsApi.getProductsByKeyword(productType.productName, getUserToken())) {
            is NetworkResponse.Success -> {
                val mappedItems = response.body.products.map { productMapper.map(it) }
                ResponseWrapper.Success(mappedItems)
            }
            else -> {
                ResponseWrapper.Error(Throwable("Something went wrong..."))
            }
        }
    }

    override suspend fun getProductDetails(productId: String): ResponseWrapper<ProductDetailsEntity> {
        return when (val response = productsApi.getProductDetails(productId)) {
            is NetworkResponse.Success -> {
                val mappedItems = productDetailsMapper.map(response.body)
                ResponseWrapper.Success(mappedItems)
            }
            else -> {
                ResponseWrapper.Error(Throwable("Something went wrong..."))
            }
        }
    }
}
