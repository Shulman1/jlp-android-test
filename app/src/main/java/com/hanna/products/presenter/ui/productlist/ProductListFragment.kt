package com.hanna.products.presenter.ui.productlist

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.hanna.products.R
import com.hanna.products.data.api.Resource
import com.hanna.products.databinding.FragmentProductListBinding
import com.hanna.products.domain.models.dto.model.ProductType
import com.hanna.products.presenter.alerts.AlertDialogs
import com.hanna.products.presenter.viewmodel.ProductListViewModel
import com.hanna.products.utils.viewBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ProductListFragment : Fragment(R.layout.fragment_product_list) {

    private val viewModel by viewModels<ProductListViewModel>()

    private val adapter = ProductsRecyclerViewAdapter()

    private val binding: FragmentProductListBinding by viewBinding(FragmentProductListBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.productName.text = ProductType.DISHWASHERS.name
        binding.productsRecyclerView.adapter = adapter
        binding.productsRecyclerView.layoutManager = GridLayoutManager(
            requireContext(), resources.getInteger(R.integer.products_column_count)
        )
        viewModel.productsSummary.observe(viewLifecycleOwner) {
            binding.loadingPage.root.isVisible = it.status == Resource.Status.LOADING
            adapter.submitList(it.data.orEmpty())
            when (it.status) {
                Resource.Status.ERROR -> {
                    AlertDialogs.showSimpleAlert(requireContext(), it.message.orEmpty()){
                        viewModel.fetchProducts()
                    }
                }
                else -> { }
            }
        }
    }
}