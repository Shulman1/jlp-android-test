package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Details(

    @SerializedName("features") var features: ArrayList<Features> = arrayListOf(),
    @SerializedName("buyingGuides") var buyingGuides: ArrayList<BuyingGuides> = arrayListOf(),
    @SerializedName("editorsNotes") var editorsNotes: String? = null,
    @SerializedName("featuredArticles") var featuredArticles: ArrayList<FeaturedArticles> = arrayListOf(),
    @SerializedName("returns") var returns: String? = null,
    @SerializedName("productInformation") var productInformation: String? = null

)