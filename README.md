# JLP Android Engineer Test

## Brief

Your task is to create an Android App that will allow customers to see the range of dishwashers on sale at John Lewis. This app should be simple to use and work on both phones and tablets.

You should build the application in Kotlin.

You are expected to provide adequate unit tests.

Please fork this repo into your own GitLab namespace. (You will need a GitLab account.)

## Product Grid

On initial app launch the customer should be presented with a grid of dishwashers available for purchase at John Lewis.

## Product Page

When a dishwasher is tapped on the product grid a new screen is displayed that displays the details about that particular product.

## Data

You can call these APIs:
`https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI`

`https://api.johnlewis.com/mobile-apps/api/v1/products/{productId}`

or use the mock data provided in `/mockdata`

## Designs

In the `/designs` folder you can find example designs of how the pages might look:

- product-page-compact.png
- product-page-regular.png
- product-grid.png

## What we are looking for

- A TDD approach to your work were appropriate.
- Appropriate levels of testing.
- Works across phone and tablet.
- Any assumptions, notes, instructions or improvement suggestions added to your README
- We assess coding choices, how you've approached the task and factors like accessibility, performance and security.

# Suggested Architecture: MVVM + Clean
As previously recommended by Google, I have decided to use the MVVM achitecture.
I have also added some usecases, and advised by Uncle Bob.

Some additional notes:
Mappers - in order to create a strict separation, between data that's returned by the server to the
entities that are being used in the app, the mapper comes to serve this purpose.

Hilt (DI)- a library supported by google,that wraps dagger, to give an easy way to inject dependencies.

Coroutines (Async) - the suggested and recommended library,for handling async tasks. This supports
the api requests

Tha above decisions are coming to support the ability to maintain, fix, read, and scale the project, in really easy manner.A


PS
For consistency purposes, I would have had some data returned from the server in different ways,
such as being consistent of how to build the pricing display.

Still to add:
-Slide of all images
-Some missing tests
-Accesibility - can do with some color contrast updates to support the app fully
-perhaps define orientaion for tablets,

As I am new to TDD, I have had some parts of the app written in tdd, and some not.
Can admit, its a little more challenging, but was nice getting a taste of the real benefit of TDD.

And obviously, there always what to improve :)

