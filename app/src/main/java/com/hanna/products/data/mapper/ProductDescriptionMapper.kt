package com.hanna.products.data.mapper

import com.hanna.products.domain.models.dto.model.ProductDetailsResponse
import com.hanna.products.domain.models.entities.ProductDetailsEntity
import java.util.*
import javax.inject.Inject

class ProductDescriptionMapper @Inject constructor() {

    fun map(productDescriptionResponse: ProductDetailsResponse): ProductDetailsEntity {
        return ProductDetailsEntity(
            images = productDescriptionResponse.media?.images?.urls.orEmpty().toList().map {
                "https:$it"
            },
            productTitle = productDescriptionResponse.title.orEmpty(),
            displayedProductPrice = "${Currency.getInstance(productDescriptionResponse.price?.currency?: "GBP").symbol}${productDescriptionResponse.price?.now}",
            productInformation = productDescriptionResponse.details?.productInformation.orEmpty(),
            productAttributes = productDescriptionResponse.dynamicAttributes,
            displaySpecialOffer = productDescriptionResponse.displaySpecialOffer.orEmpty(),
            services = productDescriptionResponse.additionalServices,
            productId = productDescriptionResponse.productId.orEmpty()
        )
    }
}