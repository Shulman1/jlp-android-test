package com.hanna.products.data.network

data class NetworkErrorMessage(val message: String, val code: Int)