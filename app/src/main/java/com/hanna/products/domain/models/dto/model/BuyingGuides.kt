package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class BuyingGuides(

    @SerializedName("linkUrl") var linkUrl: String? = null,
    @SerializedName("title") var title: String? = null

)