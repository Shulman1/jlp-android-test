package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Price(

    @SerializedName("then1") var then1: String? = null,
    @SerializedName("uom") var uom: String? = null,
    @SerializedName("currency") var currency: String? = null,
    @SerializedName("then2") var then2: String? = null,
    @SerializedName("now") var now: String? = null,
    @SerializedName("was") var was: String? = null

)