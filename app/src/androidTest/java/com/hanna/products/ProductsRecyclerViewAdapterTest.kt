package com.hanna.products

import android.view.LayoutInflater
import androidx.test.annotation.UiThreadTest
import androidx.test.platform.app.InstrumentationRegistry
import com.google.common.truth.Truth.assertThat
import com.hanna.products.databinding.ItemProductBinding
import com.hanna.products.domain.models.entities.ProductSummary
import com.hanna.products.presenter.ui.productlist.ProductsRecyclerViewAdapter
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.spy
import org.mockito.Mockito.verify

class ProductsRecyclerViewAdapterTest {

    val adapter = ProductsRecyclerViewAdapter()
    val diffUtil = ProductsRecyclerViewAdapter.diffUtil
    lateinit var itemProductView: ItemProductBinding

    @Before
    fun setup() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_product, null)
        itemProductView = ItemProductBinding.bind(itemView)
    }

    //diff util tests
    @Test
    fun whenItemContentsAreTheSameAreContentsTheSameReturnsTrue() {
        val oldItem = ProductSummary(productId = "oldId", "title", "image", "price")
        val newItem = ProductSummary(productId = "newId", "title", "image", "price")

        assertThat(diffUtil.areContentsTheSame(oldItem, newItem)).isTrue()
    }

    @Test
    fun whenItemContentsTitlesAreNotTheSameAreContentsTheSameReturnsFalse() {
        val oldItem = ProductSummary(productId = "oldId", "title1", "image", "price")
        val newItem = ProductSummary(productId = "newId", "title2", "image", "price")

        assertThat(diffUtil.areContentsTheSame(oldItem, newItem)).isFalse()
    }

    @Test
    fun whenItemContentsImagesAreNotTheSameAreContentsTheSameReturnsFalse() {
        val oldItem = ProductSummary(productId = "oldId", "title", "image1", "price")
        val newItem = ProductSummary(productId = "newId", "title", "image2", "price")

        assertThat(diffUtil.areContentsTheSame(oldItem, newItem)).isFalse()
    }

    @Test
    fun whenItemContentsProcesAreNotTheSameAreContentsTheSameReturnsFalse() {
        val oldItem = ProductSummary(productId = "oldId", "title", "image", "price1")
        val newItem = ProductSummary(productId = "newId", "title", "image", "price2")

        assertThat(diffUtil.areContentsTheSame(oldItem, newItem)).isFalse()
    }

    @Test
    @UiThreadTest
    fun productTileIsDisplayedCorrectlyOnAdapterItem() {
        adapter.submitList(listOf(ProductSummary("id", "title", "image", "price_display")))
        val viewHolder = adapter.ProductViewHolder(itemProductView)
        adapter.onBindViewHolder(viewHolder, 0)

        assertThat(itemProductView.itemProductTitle.text).isEqualTo("title1")
    }

    @Test
    @UiThreadTest
    fun productPriceRangeIsDisplayedCorrectlyOnAdapterItem() {
        adapter.submitList(listOf(ProductSummary("id", "title", "image", "price_display")))
        val viewHolder = adapter.ProductViewHolder(itemProductView)
        adapter.onBindViewHolder(viewHolder, 0)

        assertThat(itemProductView.itemProductPrice.text).isEqualTo("price_display")
    }

    @Test
    @UiThreadTest
    fun productImageCorrectlyOnAdapterItem() {

        adapter.submitList(listOf(ProductSummary("id", "title", "image", "price_display")))
        val viewHolder = adapter.ProductViewHolder(itemProductView)
        val iLoader = spy(viewHolder)//the image loader should be private but openedfortesting but for conv I have stuck to this

        adapter.onBindViewHolder(viewHolder, 0)

        verify(iLoader).bind(ProductSummary("id", "title", "image", "price_display"))
    }
}