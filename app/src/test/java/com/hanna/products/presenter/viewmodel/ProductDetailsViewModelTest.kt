package com.hanna.products.presenter.viewmodel

import com.hanna.products.base.BaseTest
import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.domain.models.dto.model.AdditionalServices
import com.hanna.products.domain.models.entities.ProductDetailsEntity
import com.hanna.products.domain.usecases.GetProductDetailsByIdUseCase
import com.hanna.products.utils.getOrAwaitValue
import com.google.gson.internal.LinkedTreeMap
import com.hanna.products.data.api.Resource
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.*


@OptIn(ExperimentalCoroutinesApi::class)
class ProductDetailsViewModelTest : BaseTest() {

    private val useCase = mock<GetProductDetailsByIdUseCase>()
    private lateinit var viewModel: ProductDetailsViewModel

    @Before
    fun setUp() {
        viewModel = ProductDetailsViewModel(useCase)
    }

    @Test
    fun `positive get product details`() = runSuspendTest {
        val productDetailsEntity = ProductDetailsEntity(
            images = listOf("image_1", "image_2"),
            productTitle = "title",
            displayedProductPrice = "100",
            productInformation = "some info..",
            productAttributes = LinkedTreeMap<String, Any>().apply {
                put("key_1", 1)
                put("key_2", "value_dynamic")
                put("key_3", 1.5)
            },
            displaySpecialOffer = "special",
            services = AdditionalServices(),
            productId = "101"
        )
        whenever(useCase(any())).thenReturn(ResponseWrapper.Success(productDetailsEntity))
        viewModel.fetchProductDetails("101")
        val stateLoading = viewModel.productInfo.getOrAwaitValue()
        assertEquals("check response is loading",stateLoading.status, Resource.Status.LOADING)
        advanceUntilIdle()
        val successState = viewModel.productInfo.getOrAwaitValue()
        assertEquals("check response is success", successState.status, Resource.Status.SUCCESS)
        assertEquals("check response same data", successState.data, productDetailsEntity)
    }


    @Test
    fun `negative get product details`() = runSuspendTest {
        val errorResponse = ResponseWrapper.Error(RuntimeException("something went wrong"))
        whenever(useCase(any())).thenReturn(errorResponse)
        viewModel.fetchProductDetails("101")

        val stateLoading = viewModel.productInfo.getOrAwaitValue()
        assertEquals("check response is loading",stateLoading.status, Resource.Status.LOADING)
        advanceUntilIdle()
        val response = viewModel.productInfo.getOrAwaitValue()
        assertEquals("check response is error", response.status, Resource.Status.ERROR)
        assertEquals("check response same message", response.message, errorResponse.throwable?.message)
    }
}