package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Deliveries (

  @SerializedName("deliveryType" ) var deliveryType : String?            = null,
  @SerializedName("options"      ) var options      : ArrayList<Options> = arrayListOf()

)