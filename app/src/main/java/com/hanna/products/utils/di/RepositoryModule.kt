package com.hanna.products.utils.di

import com.hanna.products.data.api.ProductsApi
import com.hanna.products.data.mapper.ProductDescriptionMapper
import com.hanna.products.data.mapper.ProductMapper
import com.hanna.products.data.repository.ProductsRepository
import com.hanna.products.data.repository.ProductsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {

    @Provides
    fun provideProductsRepository(
        productsApi: ProductsApi,
        productMapper: ProductMapper,
        productDescriptionMapper: ProductDescriptionMapper
    ): ProductsRepository {
        return ProductsRepositoryImpl(
            productsApi,
            productMapper,
            productDescriptionMapper
        )
    }

}