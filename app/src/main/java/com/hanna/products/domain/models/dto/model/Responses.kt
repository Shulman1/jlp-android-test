package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName

data class ProductsResponse(
    @SerializedName("products")
    val products: List<ProductResponse>
)

data class ProductResponse(

    @SerializedName("productId")
    val productId: String,

    @SerializedName("title")
    val title: String,

    @SerializedName("image")
    val image: String,

    @SerializedName("variantPriceRange")
    val variantPriceRange: VariantPriceRangeResponse

)

data class VariantPriceRangeResponse(

    @SerializedName("display")
    val display: DisplayResponse

)

data class DisplayResponse(

    @SerializedName("max")
    val max: String,

    @SerializedName("min")
    val min: String?

)


data class ProductDescriptionResponse(

    @SerializedName("type")
    val details: String

)

data class DetailsResponse(

    @SerializedName("productInformation")
    val productInformation: String

)


