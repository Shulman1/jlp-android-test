package com.hanna.products.data.repository

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.hanna.products.base.BaseTest
import com.hanna.products.data.api.ProductsApi
import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.data.mapper.ProductDescriptionMapper
import com.hanna.products.data.mapper.ProductMapper
import com.hanna.products.data.network.NetworkResponse
import com.hanna.products.domain.models.dto.model.ProductDetailsResponse
import com.hanna.products.domain.models.dto.model.ProductType
import com.hanna.products.domain.models.dto.model.ProductsResponse
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.*

@OptIn(ExperimentalCoroutinesApi::class)
class ProductsRepositoryImplTest : BaseTest() {

    private val productApi = mock<ProductsApi>()
    private val productMapper = spy<ProductMapper>()
    private val productDetailsMapper = spy<ProductDescriptionMapper>()
    private lateinit var productRepository: ProductsRepositoryImpl

    @Before
    fun setUp() {
        productRepository = ProductsRepositoryImpl(productApi, productMapper, productDetailsMapper)
    }

    @Test
    fun `positive get product summery`() = runSuspendTest {
        val productsBody = loadProducts()
        val productResponse = NetworkResponse.Success(productsBody)
        whenever(productApi.getProductsByKeyword(any(), any())).thenReturn(productResponse)
        val response = productRepository.getProductsSummary(ProductType.DISHWASHERS)
        verify(productMapper, times(productsBody.products.size)).map(any())
        assertEquals("check is success", response::class.java, ResponseWrapper.Success::class.java)
        val result = productResponse.body.products.map { productMapper.map(it) }
        assertEquals("same size list", result.size, productsBody.products.size)
        for (i in result.indices) {
            val expectedItem = (response as ResponseWrapper.Success).value[i]
            val actualItem = result[i]
            assertEquals("same item", expectedItem, actualItem)
        }
    }

    @Test
    fun `negative get product summery`() = runSuspendTest {
        val productResponse = NetworkResponse.Error(RuntimeException("Failed!!"))
        whenever(productApi.getProductsByKeyword(any(), any())).thenReturn(productResponse)
        val response = productRepository.getProductsSummary(ProductType.DISHWASHERS)
        verify(productMapper, never()).map(any())
        assertEquals("check is error", response::class.java, ResponseWrapper.Error::class.java)
        assertEquals(
            "check message",
            (response as ResponseWrapper.Error).throwable?.message,
            "Something went wrong..."
        )
    }

    @Test
    fun `positive get product details`() = runSuspendTest {
        val productDetails = loadProductDetails()
        whenever(productApi.getProductDetails(any())).thenReturn(
            NetworkResponse.Success(
                productDetails
            )
        )
        val response = productRepository.getProductDetails("101")
        verify(productDetailsMapper).map(any())
        val resultMapper = productDetailsMapper.map(productDetails)
        assertEquals("check is success", response::class.java, ResponseWrapper.Success::class.java)
        assertEquals("check is success", (response as ResponseWrapper.Success).value, resultMapper)
    }

    @Test
    fun `negative get product details`() = runSuspendTest {
        val productResponse = NetworkResponse.Error(RuntimeException("Failed!!"))
        whenever(productApi.getProductDetails(any())).thenReturn(productResponse)
        val response = productRepository.getProductDetails("101")
        verify(productDetailsMapper, never()).map(any())
        assertEquals("check is error", response::class.java, ResponseWrapper.Error::class.java)
        assertEquals(
            "check message",
            (response as ResponseWrapper.Error).throwable?.message,
            "Something went wrong..."
        )
    }

    private fun loadProducts(): ProductsResponse {
        return javaClass.classLoader?.getResource("data.json")!!.readText()
            .let { Gson().fromJson(it, object : TypeToken<ProductsResponse>() {}.type) }
    }

    private fun loadProductDetails(): ProductDetailsResponse {
        return javaClass.classLoader?.getResource("ProductDetailsJSONResponseMock.json")!!
            .readText()
            .let { Gson().fromJson(it, object : TypeToken<ProductDetailsResponse>() {}.type) }
    }
}