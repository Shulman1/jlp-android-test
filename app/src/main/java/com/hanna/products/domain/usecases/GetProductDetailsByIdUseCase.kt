package com.hanna.products.domain.usecases

import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.domain.models.entities.ProductDetailsEntity
import com.hanna.products.data.repository.ProductsRepository
import javax.inject.Inject

class GetProductDetailsByIdUseCase @Inject constructor(private val productsRepository: ProductsRepository) {

    suspend operator fun invoke(productId: String): ResponseWrapper<ProductDetailsEntity> {
        return productsRepository
            .getProductDetails(productId)
    }
}