package com.hanna.products.domain.usecases

import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.domain.models.dto.model.ProductType
import com.hanna.products.domain.models.entities.ProductSummary
import com.hanna.products.data.repository.ProductsRepository
import javax.inject.Inject

class GetProductsSummaryUseCase @Inject constructor(private val productsRepository: ProductsRepository) {

    suspend operator fun invoke(selectedProductType: ProductType): ResponseWrapper<List<ProductSummary>> {
        return productsRepository
            .getProductsSummary(selectedProductType)
    }
}