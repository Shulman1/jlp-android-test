package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class OptionalServices (

  @SerializedName("description"         ) var description         : String?           = null,
  @SerializedName("id"                  ) var id                  : String?           = null,
  @SerializedName("price"               ) var price               : String?           = null,
  @SerializedName("orderOnSite"         ) var orderOnSite         : Int?              = null,
  @SerializedName("customProperties"    ) var customProperties    : CustomProperties? = CustomProperties(),
  @SerializedName("title"               ) var title               : String?           = null,
  @SerializedName("associatedProductId" ) var associatedProductId : String?           = null,
  @SerializedName("type"                ) var type                : String?           = null

)