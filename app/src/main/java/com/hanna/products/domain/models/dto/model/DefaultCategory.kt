package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class DefaultCategory (

  @SerializedName("id"   ) var id   : String? = null,
  @SerializedName("name" ) var name : String? = null

)