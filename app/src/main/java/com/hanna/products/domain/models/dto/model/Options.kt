package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Options(
    @SerializedName("price") var price: String? = null,
    @SerializedName("newShortDescription") var newShortDescription: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("newStandardDescription") var newStandardDescription: String? = null,
    @SerializedName("isApprovedSupplier") var isApprovedSupplier: Boolean? = null,
    @SerializedName("standardDescription") var standardDescription: String? = null,
    @SerializedName("newPriority") var newPriority: Int? = null,
    @SerializedName("shortDescription") var shortDescription: String? = null
)