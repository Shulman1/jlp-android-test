package com.hanna.products.data.mapper

import com.google.common.truth.Truth.assertThat
import com.hanna.products.domain.models.dto.model.DisplayResponse
import com.hanna.products.domain.models.dto.model.ProductResponse
import com.hanna.products.domain.models.dto.model.VariantPriceRangeResponse
import org.junit.Test

class ProductMapperTest {

    private val mapper = ProductMapper()

    private val response = ProductResponse(
        productId = "123abc",
        title = "Great Dishwasher",
        image = "abc_url",
        variantPriceRange = VariantPriceRangeResponse(DisplayResponse("150", "100"))
    )

    @Test
    fun `GIVEN a ProductMapper WHEN ProductDetailsResponse THEN mapped object has correct productId`() {
        val productSummary =
            mapper.map(response.copy(productId = "12345-dishwasher"))


        assertThat(productSummary.productId).isEqualTo("12345-dishwasher")
    }

    @Test
    fun `GIVEN a ProductMapper WHEN ProductDetailsResponse THEN mapped object has correct title`() {
        val productSummary =
            mapper.map(response.copy(title = "This is a title"))


        assertThat(productSummary.title).isEqualTo("This is a title")
    }

    @Test
    fun `GIVEN a ProductMapper WHEN ProductDetailsResponse THEN mapped object has correctly built urls`() {
        val productSummary =
            mapper.map(
                response.copy(
                    image = "abc_url"
                )
            )

        assertThat(productSummary.imageUrl).isEqualTo("https:abc_url")
    }

    @Test
    fun `GIVEN a ProductMapper WHEN ProductDetailsResponse THEN mapped object has displaying price`() {
        val productSummary =
            mapper.map(
                response.copy(
                    variantPriceRange = VariantPriceRangeResponse(
                        DisplayResponse(
                            "150",
                            "100"
                        )
                    )
                )
            )

        assertThat(productSummary.displayPrice).isEqualTo("100-150")
    }
}