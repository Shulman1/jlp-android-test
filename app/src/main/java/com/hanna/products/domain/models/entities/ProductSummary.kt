package com.hanna.products.domain.models.entities

data class ProductSummary(
    val productId: String,
    val title: String,
    val imageUrl: String,
    val displayPrice: String
)