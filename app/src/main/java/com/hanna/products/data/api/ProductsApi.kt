package com.hanna.products.data.api

import com.hanna.products.domain.models.dto.model.ProductDetailsResponse
import com.hanna.products.domain.models.dto.model.ProductsResponse
import com.hanna.products.data.network.NetworkResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ProductsApi {

    @GET("/search/api/rest/v2/catalog/products/search/keyword")
    suspend fun getProductsByKeyword( //reason for this name is due to the endpoint provided
        @Query("q") productName: String,
        @Query("key") key: String//usually tokens should be attached the header so the token is attached to all calls in the project/app
    ): NetworkResponse<ProductsResponse>

    @GET("/mobile-apps/api/v1/products/{productId}")
    suspend fun getProductDetails(@Path("productId") productId: String): NetworkResponse<ProductDetailsResponse>

}