package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Skus(
    @SerializedName("media") var media: Media? = Media(),
    @SerializedName("swatchUrl") var swatchUrl: String? = null,
    @SerializedName("code") var code: String? = null,
    @SerializedName("availability") var availability: Availability? = Availability(),
    @SerializedName("priceBand") var priceBand: String? = null,
    @SerializedName("dynamicAttributes") var dynamicAttributes: DynamicAttributes? = DynamicAttributes(),
    @SerializedName("size") var size: String? = null,
    @SerializedName("brandName") var brandName: String? = null,
    @SerializedName("sizeHeadline") var sizeHeadline: String? = null,
    @SerializedName("ticketType") var ticketType: String? = null,
    @SerializedName("skuTitle") var skuTitle: String? = null,
    @SerializedName("unitPriceInfo") var unitPriceInfo: UnitPriceInfo? = UnitPriceInfo(),
    @SerializedName("price") var price: Price? = Price(),
    @SerializedName("id") var id: String? = null,
    @SerializedName("color") var color: String? = null
)