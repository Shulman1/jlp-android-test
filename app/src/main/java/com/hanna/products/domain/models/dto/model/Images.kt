package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Images (
  @SerializedName("urls" ) var urls : ArrayList<String> = arrayListOf()
)