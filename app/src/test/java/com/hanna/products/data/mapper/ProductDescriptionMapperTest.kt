package com.hanna.products.data.mapper

import com.google.common.truth.Truth.assertThat
import com.google.gson.internal.LinkedTreeMap
import com.hanna.products.domain.models.dto.model.*
import org.junit.Test

class ProductDescriptionMapperTest {

    private val mapper = ProductDescriptionMapper()

    private val response = ProductDetailsResponse(
        legs = arrayListOf(),
        ageRestriction = 3,
        media = Media(images = Images(urls = arrayListOf("//image_url"))),
        brand = Brand("logo", "Meile"),
        type = null,
        dynamicAttributes = LinkedTreeMap(), price = Price(now = "456", currency = null)
    )

    @Test
    fun `GIVEN a ProductDescriptionMapper WHEN ProductDetailsResponse is passed THEN mapped object has mapped urls`() {
        val imagesList = arrayListOf(
            "//image_url",
            "//image_url2"
        )
        val productDetailsEntity =
            mapper.map(response.copy(media = Media(images = Images(imagesList))))

        productDetailsEntity.images.forEachIndexed { index, s ->
            assertThat(s).isEqualTo("https:${imagesList[index]}")
        }
    }


    @Test
    fun `GIVEN a ProductDescriptionMapper WHEN ProductDetailsResponse has not price currency THEN mapped object is mapped to GBP`() {
        val productDetailsEntity =
            mapper.map(response.copy(price = Price(now = "456", currency = null)))

        val displayedPrice = productDetailsEntity.displayedProductPrice
        assertThat(displayedPrice).isEqualTo("£456")
    }

    @Test
    fun `GIVEN a ProductDescriptionMapper WHEN ProductDetailsResponse has price currency THEN mapped object is mapped to GBP`() {
        val productDetailsEntity =
            mapper.map(response.copy(price = Price(now = "456", currency = "USD")))

        val displayedPrice = productDetailsEntity.displayedProductPrice
        assertThat(displayedPrice).isEqualTo("$456")
    }

    //TODO(Add tests for each field of this mapper)
}