package com.hanna.products.presenter.viewmodel

import com.hanna.products.base.BaseTest
import com.hanna.products.data.api.Resource
import com.hanna.products.data.api.ResponseWrapper
import com.hanna.products.domain.models.entities.ProductSummary
import com.hanna.products.domain.usecases.GetProductsSummaryUseCase
import com.hanna.products.utils.getOrAwaitValue
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever


@OptIn(ExperimentalCoroutinesApi::class)
class ProductListViewModelTest : BaseTest() {

    private val useCase = mock<GetProductsSummaryUseCase>()
    private lateinit var viewModel: ProductListViewModel

    @Test
    fun `positive get product list`() = runSuspendTest {
        val productList = listOf(
            ProductSummary("101","title1","image1", displayPrice = "100"),
            ProductSummary("102","title2","image2", displayPrice = "200"),
            ProductSummary("103","title3","image3", displayPrice = "300"),
        )
        whenever(useCase(any())).thenReturn(ResponseWrapper.Success(productList))
        viewModel = ProductListViewModel(useCase)
        val state1 = viewModel.products.getOrAwaitValue()
        assertEquals("check response is loading", state1.status, Resource.Status.LOADING)
        advanceUntilIdle()
        val state2 = viewModel.products.getOrAwaitValue()
        assertEquals("check response is success", state2.status, Resource.Status.SUCCESS)
        assertEquals("check response is data", state2.data, productList)
    }

    @Test
    fun `negative get product list`() = runSuspendTest {
        val errorResponse = ResponseWrapper.Error(RuntimeException("something went wrong"))
        whenever(useCase(any())).thenReturn(errorResponse)
        viewModel = ProductListViewModel(useCase)
        val state1 = viewModel.products.getOrAwaitValue()
        assertEquals("check response is loading", state1.status, Resource.Status.LOADING)
        advanceUntilIdle()
        val state2 = viewModel.products.getOrAwaitValue()
        assertEquals("check response is error", state2.status, Resource.Status.ERROR)
        assertEquals("check response is error message", state2.message, errorResponse.throwable?.message)
    }
}