package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Attributes(

    @SerializedName("value") var value: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("values") var values: ArrayList<String> = arrayListOf(),
    @SerializedName("multivalued") var multivalued: Boolean? = null

)