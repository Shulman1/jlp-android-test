package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Availability(

    @SerializedName("isPreorder") var isPreorder: Boolean? = null,
    @SerializedName("stockLevel") var stockLevel: Int? = null,
    @SerializedName("message") var message: String? = null,
    @SerializedName("availabilityStatus") var availabilityStatus: String? = null

)