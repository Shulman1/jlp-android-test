package com.hanna.products.presenter.alerts

import android.app.AlertDialog
import android.content.Context

object AlertDialogs {

    fun showSimpleAlert(context: Context, message: String, onAction: () -> Unit) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("An Error!")
        builder.setMessage((message))
        builder.setNegativeButton("RETRY") { dialogInterface, which ->
            dialogInterface.dismiss()
            onAction()
        }
        builder.create()
        builder.show()
    }
}