package com.hanna.products.domain.models.dto.model

import com.google.gson.annotations.SerializedName


data class Features (

  @SerializedName("groupName"  ) var groupName  : String?               = null,
  @SerializedName("attributes" ) var attributes : ArrayList<Attributes> = arrayListOf()

)