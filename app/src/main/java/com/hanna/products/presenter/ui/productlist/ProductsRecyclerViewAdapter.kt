package com.hanna.products.presenter.ui.productlist

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.hanna.products.domain.models.entities.ProductSummary
import com.hanna.products.databinding.ItemProductBinding
import com.hanna.products.presenter.ui.BindableViewHolder
import com.hanna.products.presenter.ui.productdetails.ProductDetailsActivity
import com.hanna.products.presenter.ui.productdetails.ProductDetailsActivity.Companion.EXTRA_PRODUCT_INFO_ID
import com.hanna.products.utils.di.ImageModule
import com.hanna.products.utils.di.ImageModule_ProvideImageLoaderFactory.provideImageLoader

class ProductsRecyclerViewAdapter :
    ListAdapter<ProductSummary, BindableViewHolder<ProductSummary>>(diffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {

        val binding: ItemProductBinding =
            ItemProductBinding.inflate(LayoutInflater.from(parent.context))
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BindableViewHolder<ProductSummary>, position: Int) {
        holder.bind(currentList[position])
    }

    companion object{
        val diffUtil = object :
            DiffUtil.ItemCallback<ProductSummary>() {
            override fun areItemsTheSame(
                oldItem: ProductSummary,
                newItem: ProductSummary
            ): Boolean {
                return oldItem.productId == newItem.productId
            }

            override fun areContentsTheSame(
                oldItem: ProductSummary,
                newItem: ProductSummary
            ): Boolean {
                return oldItem.title == newItem.title && oldItem.displayPrice == newItem.displayPrice && oldItem.imageUrl == newItem.imageUrl
            }
        }
    }

    inner class ProductViewHolder (
        private val binding: ItemProductBinding,
    ) : BindableViewHolder<ProductSummary>(binding.root) {

        var imageLoader = provideImageLoader(ImageModule())//ImageModule//provideImageLoader(ImageModule())

        override fun bind(data: ProductSummary) {

            imageLoader.loadImage(data.imageUrl, binding.itemProductImage)

            //accessibility
            binding.itemProductImage.contentDescription = "Image of ${data.title}"

            binding.itemProductTitle.text = data.title
            binding.itemProductPrice.text = data.displayPrice
            binding.itemProduct.setOnClickListener {
                val intent = Intent(itemView.context, ProductDetailsActivity::class.java)
                intent.putExtra(EXTRA_PRODUCT_INFO_ID, data.productId)
                itemView.context.startActivity(intent)
            }
        }
    }
}

